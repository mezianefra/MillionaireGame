package com.meziane.millionairegame.room

import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "gameTable")
class GameTable (
    @PrimaryKey(autoGenerate = true)
    val id: Int = 1,
    val category: String,
    val correct_answer: String,
    val difficulty: String,
    val incorrect_answers: List<String>?,
    val question: String,
    val type: String

)

