package com.meziane.millionairegame.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface TableQueries {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addrow(data: List<GameTable?>)


    @Query("SELECT * FROM gameTable")
    fun getTableData(): List<GameTable>
}