package com.meziane.millionairegame.module

import android.content.Context
import com.google.gson.GsonBuilder
import com.meziane.millionairegame.datastore.DataPref
import com.meziane.millionairegame.datastore.PrefImpl
import com.meziane.millionairegame.model.Api
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)

object ApiModule {

    @Provides
    @Singleton
    fun retrofiIstance(): Api{

        val interceptor = HttpLoggingInterceptor()
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
        val okHttpClient = OkHttpClient.Builder().addInterceptor(interceptor).build()

        return (
                Retrofit
                    .Builder()
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl("https://opentdb.com/")
                    .client(okHttpClient)
                    .build()
                    .create(Api::class.java))


    }

    @Singleton
    @Provides
    fun provideDataStore(@ApplicationContext context: Context): DataPref = PrefImpl(context)
}




