package com.meziane.millionairegame.datastore

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

private val Context.dataStore: DataStore<androidx.datastore.preferences.core.Preferences> by preferencesDataStore(name = "user")

class PrefImpl @Inject constructor(private val context: Context) : DataPref {

    private val USER_KEY = stringPreferencesKey("username")
    override suspend fun setToken(value: String) {

        context.dataStore.edit { it[USER_KEY] = value }
    }

    override suspend fun getToken(): Flow<String> {
        return context.dataStore.data.map { it[USER_KEY] ?: "" }
    }

    override suspend fun deleteToken() {
        try {
            context.dataStore.edit {
                it.remove(USER_KEY)
            }
        } catch (e: Exception) {
            e.localizedMessage
        }
    }
}